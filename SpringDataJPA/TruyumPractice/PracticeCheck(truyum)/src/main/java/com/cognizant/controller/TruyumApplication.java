package com.cognizant.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.cognizant.util.ConnectionHandler;


@SpringBootApplication(scanBasePackages = {"com.cognizant.controller","com.cognizant.dao","com.cognizant.util","com.cognizant.model"})
public class TruyumApplication {
	public static void main(String[] args) {
		SpringApplication.run(TruyumApplication.class, args);
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(ConnectionHandler.class);
	}

}
