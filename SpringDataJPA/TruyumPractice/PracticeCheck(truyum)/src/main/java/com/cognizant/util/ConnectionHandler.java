package com.cognizant.util;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
public class ConnectionHandler {

	@Bean
	public InternalResourceViewResolver resolver() {
	    InternalResourceViewResolver resource = new InternalResourceViewResolver();
	    resource.setPrefix("/WEB-INF/views/");
	    resource.setSuffix(".jsp");
	    return resource;
	}
	

}
