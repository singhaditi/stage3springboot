package com.cognizant.dao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.cognizant.model.Cart;
import com.cognizant.model.MenuItem;
import com.cognizant.repository.CartDao;
import com.cognizant.repository.MenuItemDao;

@Service
public class CartDaoSqlImpl {
	
	@Autowired
	CartDao cartDao;
	
	@Autowired
	MenuItemDao menuItemDao;

	@Transactional
	public void addCartItem(int userId, int menuItemId) {
		Cart cart=new Cart(userId, menuItemId);
		cartDao.save(cart);
	}

	@Transactional
	public List<MenuItem> getAllCartItems(int userId) throws CartEmptyException {
		List<Integer> list=cartDao.getAllCartItems(userId);
		List<MenuItem> menu=new ArrayList<MenuItem>();
		for (Integer abc : list) {
			MenuItem item=menuItemDao.findById(abc).get();
			menu.add(item);
		}
		return menu;
	}

	@Transactional
	public void removeCartItem(int userId, int menuItemId) {
        cartDao.removeCartItem(userId, menuItemId);
	}

}
