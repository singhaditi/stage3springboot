package com.cognizant.dao;
import java.awt.Menu;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.cognizant.model.MenuItem;
import com.cognizant.repository.MenuItemDao;

@Service
public class MenuItemDaoSqlImpl{
	
	@Autowired
	MenuItemDao menuItemDao;

	@Transactional
	public List<MenuItem> getMenuItemListAdmin() {
		List<MenuItem> menu=menuItemDao.getMenuItemListAdmin();
		return menu;
	}

	@Transactional
	public List<MenuItem> getMenuItemListCustomer() {
		List<MenuItem> menu=menuItemDao.getMenuItemListCustomer();
			return menu;
	}

	@Transactional
	public void modifyMenuItem(MenuItem menuItem) {
		Optional<MenuItem> res=menuItemDao.findById(menuItem.getId());
		MenuItem menuListt = res.get();
		menuListt.setActive(menuItem.isActive());
		menuListt.setCategory(menuItem.getCategory());
		menuListt.setDateOfLaunch(menuItem.getDateOfLaunch());
		menuListt.setFreeDelivery(menuItem.isFreeDelivery());
		menuListt.setName(menuItem.getName());
		menuListt.setPrice(menuItem.getPrice());
		menuItemDao.save(menuListt);
	}

	@Transactional
	public MenuItem getMenuItem(long menuItemId) {
		return null;
	}

}
