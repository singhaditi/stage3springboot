package com.Singleton;
public class UseSingletonMain {
	public static void main(String[] args) {

		DBConn abc = DBConn.getInstance();
		System.out.println(abc);

		DBConn abcd= DBConn.getInstance();
		System.out.println(abcd);

		System.out.println(abc == abcd);

	}

}
