package com.Singleton;


public class DBConn {

	private static DBConn conn= null;

	private DBConn() {

	}

	public static DBConn getInstance() {
        if (conn== null) {
			conn= new DBConn();
		}
		return conn;
	}

}
