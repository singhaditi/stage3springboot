package com.AbstractFactory;

public abstract class HeadLight {
	
	protected final String headLightModel;

	public HeadLight(String headLightModel) {
		headLightModel = headLightModel;
	}

	public String getHeadLightModel() {
		return headLightModel;
	}
}